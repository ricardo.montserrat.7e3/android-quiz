package cat.itb.androidquiz;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    TextView question;
    TextView questionProgressText;

    QuestionModel[] questions;
    int pointer;

    ProgressBar progressBar;
    AlertDialog.Builder alert;

    private void loadQuestion()
    {

        question.setText(questions[pointer].getQuestionTextRef());

        String progressText = "Question " + (pointer + 1) + " out of " + questions.length;
        questionProgressText.setText(progressText);

        progressBar.setProgress(pointer*100/questions.length);
    }

    @Override
    public void onClick(View v)
    {
        boolean userAnswer = ((Button)v).getText().toString().equals("True");
        int msg = userAnswer == questions[pointer].getAnswer()? R.string.correct : R.string.incorrect;
        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();

        if((pointer + 1) == questions.length)
            alert.show();
        else
        {
            pointer=(pointer+1)%questions.length;
            loadQuestion();
        }
    }

    private void setAlertDialog()
    {
        alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle("Congratulations, you finished the quiz!");
        alert.setMessage("What do you want to do next?");

        alert.setPositiveButton("Restart", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                finish();
                startActivity(getIntent());
            }
        });

        alert.setNegativeButton("Finish", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                finish();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        questions = new QuestionModel[]{
                new QuestionModel(R.string.question_1, true),
                new QuestionModel(R.string.question_2, true),
                new QuestionModel(R.string.question_3, false),
                new QuestionModel(R.string.question_4, false),
                new QuestionModel(R.string.question_5, false),
                new QuestionModel(R.string.question_6, false),
                new QuestionModel(R.string.question_7, true),
                new QuestionModel(R.string.question_8, false),
                new QuestionModel(R.string.question_9, true),
                new QuestionModel(R.string.question_10, false),
        };

        pointer = 0;

        progressBar = findViewById(R.id.progressBar);

        question = findViewById(R.id.question);
        questionProgressText = findViewById(R.id.questionProgressText);

        findViewById(R.id.true_button).setOnClickListener(this);
        findViewById(R.id.false_button).setOnClickListener(this);

        setAlertDialog();

        loadQuestion();
    }
}