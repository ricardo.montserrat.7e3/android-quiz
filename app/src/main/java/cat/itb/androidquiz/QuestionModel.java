package cat.itb.androidquiz;

public class QuestionModel
{
    private int questionTextRef;
    private Boolean answer;

    public QuestionModel(int questionTextRef, Boolean answer) {
        this.questionTextRef = questionTextRef;
        this.answer = answer;
    }

    public int getQuestionTextRef() {
        return questionTextRef;
    }

    public void setQuestionTextRef(int questionTextRef) {
        this.questionTextRef = questionTextRef;
    }

    public Boolean getAnswer() {
        return answer;
    }

    public void setAnswer(Boolean answer) {
        this.answer = answer;
    }
}
